const handler = require("./api/tweets/[tweets].js");
const util = require("util");

handler(
  { query: { tweets: "foo" } },
  {
    json: (input) => {
      console.log(util.inspect(input, false, null, true));
    },
  }
)
  .then(() => {
    process.exit(0);
  })
  .catch(console.error);
