const Twitter = require("../../lib/twitter");
const twitter = new Twitter();

const logger = require("pino")({
  level: process.env.NODE_ENV === "development" ? "debug" : "info",
});

const Redis = require("../../lib/redis");

const twitterHandleSelf = process.env.TWITTER_HANDLE_SELF;
const twitterHandleOperator = process.env.TWITTER_HANDLE_OPERATOR;
const twitterHandleMaintainer = process.env.TWITTER_HANDLE_MAINTAINER;

const redisClient = new Redis().client;
const replyHandleKey = "replied_to";
const replyMessages = [
  { key: "foo", msg: "foo bar baz" },
  { key: "lorem", msg: "lorem ipsum dolor sit amet" },
]; //[];

function markHandleAsRepliedTo(users) {
  users.forEach((user) => {
    if (user.screen_name === twitterHandleSelf) return;
    redisClient.sadd(replyHandleKey, user.screen_name);
  });
}

function alreadyRepliedTo(user) {
  return redisClient.sismember(replyHandleKey, user);
}

async function handleRepliesOperation(twitterClient) {
  logger.info("Processing requests for replies!");
  if (replyMessages.length === 0) {
    logger.error("No replies available, will not proceed processing!");
    throw new Error("No replies available! Check replies Data Source");
  }
  // - Search for mentions of bot by @BlondeHistorian
  logger.info(`Searching for mentions of '@${twitterHandleSelf}'`);
  const tweetsToReplyTo = await twitterClient.get("search/tweets", {
    q: `from:${twitterHandleMaintainer} OR from:${twitterHandleOperator} @${twitterHandleSelf}`,
    result_type: "recent",
    count: 100,
  });
  logger.debug(tweetsToReplyTo);
  tweetsToReplyTo.statuses.forEach((tweet) => {
    logger.debug(tweet);
    // if (tweet.user.screen_name !== twitterHandleOperator) return;
    // if (tweet.created_at older than 24h) return;
    if (alreadyRepliedTo(tweet.user.screen_name)) return;
    // TODO: Don't pick random reply. Save last N replies and pick other
    const pickedReplyMessage =
      replyMessages[Math.floor(Math.random() * replyMessages.length)];
    // TODO: Remove logger and uncomment posting reply
    logger.info(
      "Would post reply to tweet %s with content: '%s'",
      tweet.id,
      pickedReplyMessage.msg
    );
    // await twitterClient.post("statuses/update", {
    //   in_reply_to_id: tweet.id,
    //   status: pickedReplyMessage.msg,
    //   auto_populate_reply_metadata: true,
    // });
    markHandleAsRepliedTo(tweet.entities.user_mentions);
  });
  logger.info("Finished processing replies!");
}

function handlePostsOperation(twitterClient) {
  // - Check that no post has been made in specified interval
  // - Choose a post
  // - Post the post
  logger.info("Processing posts!");
}

function handleKeywordsOperation(twitterClient) {
  // - Search for questions with keywords
  // - Choose a reply
  // - Reply to question
  logger.info("Processing keywords!");
}

async function handler(req, res) {
  logger.info("Got request");
  const redisKeys = await redisClient.keys("*");
  logger.debug("Redis keys: %O", redisKeys);
  logger.debug("Show req query, %O", req.query);
  const operation = req.query.tweets;
  logger.debug("Show req query, '%s'", operation);

  await twitter.auth();

  const twitterClient = twitter.client;

  switch (operation) {
    case "replies":
      await handleRepliesOperation(twitterClient);
      break;
    case "posts":
      handlePostsOperation(twitterClient);
      break;
    case "keywords":
      handleKeywordsOperation(twitterClient);
      break;

    default:
      logger.info("No operation given.");
      await handleRepliesOperation(twitterClient);
      break;
  }

  let responseObject = [];
  return res.json(responseObject);
}

module.exports = (req, res) => {
  return handler(req, res)
    .then((response) => logger.info("Handler finished."))
    .catch((error) => logger.error(error));
};
