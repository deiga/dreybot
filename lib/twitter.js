const TwitterAPI = require("twitter-lite");

class Twitter {
  constructor() {
    this.user = new TwitterAPI({
      consumer_key: process.env.TWITTER_CONSUMER_KEY,
      consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    });
  }

  async auth() {
    const response = await this.user.getBearerToken();
    this.client = new TwitterAPI({
      bearer_token: response.access_token,
    });
  }
}

module.exports = Twitter;
