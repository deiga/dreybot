const Tedis = require("tedis").Tedis;

class Redis {
  constructor() {
    this.client = new Tedis({
      host: process.env.REDIS_HOST,
      port: Number(process.env.REDIS_PORT),
      password: process.env.REDIS_PASSWORD,
    });
  }
}

module.exports = Redis;
